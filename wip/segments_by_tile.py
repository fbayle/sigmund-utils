
import os
import subprocess
import shutil
if __name__ == '__main__':
# '0000013568-0000027136'
#'0000000000-0000000000'
    tiles = ['0000000000-0000013568',
            '0000000000-0000027136',
            '0000000000-0000040704',
            '0000013568-0000000000',
            '0000013568-0000013568',
            '0000013568-0000040704',
            '0000027136-0000000000',
            '0000027136-0000013568',
            '0000027136-0000027136',
            '0000027136-0000040704',
            '0000040704-0000000000',
            '0000040704-0000013568',
            '0000040704-0000027136',
            '0000040704-0000040704',
            '0000054272-0000000000',
            '0000054272-0000013568',
            '0000054272-0000027136',
            '0000054272-0000040704']
    tiles_er = ['0000000000-0000000000',
                '0000000000-0000013568',
                '0000000000-0000027136',
                '0000013568-0000000000',
                '0000013568-0000013568',
                '0000013568-0000027136',
                '0000027136-0000000000',
                '0000027136-0000013568',
                '0000027136-0000027136',
                '0000040704-0000000000',
                '0000040704-0000013568',
                '0000040704-0000027136']


    for tile in tiles_er:
        tifname = '{}.tif'.format(tile)
        tifpath_gs = 'gs://dym-sigmund-temp/entre-rios-sigmund-bands-time-series/2020-02-01_2020-03-01/{}'.format(tifname)
        path_segs = '/home/federico/er1920/results/segments'
        tifpath = '/home/federico/er1920/images/2020-02-01_2020-03-01/{}'.format(tifname)
        if not os.path.isfile(tifpath):
            cmd_download_images = "gsutil -m cp {} {}".format(tifpath_gs,tifpath) 
            subprocess.call(cmd_download_images, shell=True)
        # vrt_name='/home/federico/cba1920/images/{date}.vrt'.format(date=date)
        # cmd_buildvrt= "gdalbuildvrt  {vrt_name} / home/federico/cba1920/images/*.tif'.format(vrt_name=vrt_name)
        vec_segs = '{}/{}.shp'.format(path_segs, tile)
        cmd_lsms = 'otbcli_LargeScaleMeanShift -in {} -spatialr 10 -ranger 0.05 -minsize 20 -mode.vector.out {}'.format(tifpath, vec_segs)
        subprocess.call(cmd_lsms, shell=True)
        shutil.rmtree(tifpath)
        print("{} done".format(tile))
