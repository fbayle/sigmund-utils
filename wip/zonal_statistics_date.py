import os
import datetime
from dateutil.relativedelta import relativedelta
import subprocess
import shutil

if __name__ == '__main__':

    start = datetime.datetime.strptime("2019-01-01", "%Y-%m-%d")
    end = datetime.datetime.strptime("2020-03-01", "%Y-%m-%d")
    date_generated = [start + relativedelta(months=+x) for x in range(0, 15)]
    dates = [d.strftime("%Y-%m-%d") for d in date_generated]
    dates.append(end.strftime("%Y-%m-%d"))
    dates = [['{d}'.format(d=dates[d]), '{d}'.format(d=dates[d+1])]
             for d in range(0, len(dates)-1)]

    for date in dates:
        date = '{d1}_{d2}'.format(d1=date[0], d2=date[1])
        os.makedirs('/home/federico/cba1920/images/{date}'.format(date=date), exist_ok=True)
        if not os.path.isfile('/home/federico/cba1920/images/{date}/0000013568-0000027136.tif'.format(date=date)):
            cmd_download_images = "gsutil -m cp -r gs://dym-sigmund-temp/cordoba-sigmund-bands-time-series/{date}/0000013568-0000027136.tif /home/federico/cba1920/images/{date}/0000013568-0000027136.tif".format(
            date=date)
            subprocess.call(cmd_download_images, shell=True)
        # vrt_name='/home/federico/cba1920/images/{date}.vrt'.format(date=date)
        # cmd_buildvrt= "gdalbuildvrt  {vrt_name} / home/federico/cba1920/images/*.tif'.format(vrt_name=vrt_name)
        path_in = '/home/federico/cba1920/images/{date}'.format(date=date)
        raster_name = '{path}/0000013568-0000027136.tif'.format(path=path_in)
        cmd_run_zs = 'python3 run_zonal_statistics_raster.py --raster_file {raster_name} --date {date}'.format(
            raster_name=raster_name, date=date)
        subprocess.call(cmd_run_zs, shell=True)
        #shutil.rmtree('/home/federico/cba1920/images/{date}'.format(date=date))
        print("{date} done".format(date=date))
