#!/usr/bin/env python3
"""
This script reads a spatialite template, fill them and
export a csv file with segment's spectral attributes
"""

import glob
import os
import subprocess

def create_template(sqlite_in, sqlite_out, segs_shp, gt_shp, deptos_shp, dir_results, tile_name):
    segments_predictions_out = "{dir_results}/csv_prediction/{f}_predictions.csv".format(f=os.path.basename(os.path.splitext(segs_shp)[0]), dir_results=dir_results)
    segments_gt_out = "{dir_results}/training/{f}".format(f=os.path.basename(os.path.splitext(segs_shp)[0]), dir_results=dir_results)
    segments_gt_csv_out = "{dir_results}/training_csv/{f}.csv".format(f=os.path.basename(os.path.splitext(segs_shp)[0]), dir_results=dir_results)
    segments_label_out = "{dir_results}/prediction/{f}".format(f=os.path.basename(os.path.splitext(segs_shp)[0]), dir_results=dir_results)
    with open(sqlite_in) as f:
        sqlite_commands = f.read().format(segments=segs_shp, gt=gt_shp, deptos=deptos_shp, segments_gt_out=segments_gt_out ,segments_label_out=segments_label_out, segments_predictions_out=segments_predictions_out, tile_name=tile_name, segments_gt_csv=segments_gt_csv_out)
    with open(sqlite_out, "w") as text_file:
        text_file.write(sqlite_commands)
    print("{f} ready".format(f=sqlite_out))

def run_template(sqlite_out, dbname, quiet=False):
    quiet_str = ">/dev/null 2>/dev/null" if quiet else ""
    cmd_sqlite = "spatialite {db} < {sqlite_out} {quiet}".format(
        db=dbname, sqlite_out=sqlite_out, quiet=quiet_str)
    subprocess.call(cmd_sqlite, shell=True)
    os.remove(dbname)

def main(args):
    dir_results = '/home/federico/cba1920/results'
    sqlite_out = "{dir_results}/sqlite_files/{f}.sqlite".format(f=os.path.basename(os.path.splitext(args.segs_shp)[0]), dir_results=dir_results)
    dbname = "{dir_results}/sqlite_files/{f}.db".format(f=os.path.basename(os.path.splitext(args.segs_shp)[0]), dir_results=dir_results)
    print(sqlite_out)
    create_template(args.sqlite_in, sqlite_out, args.segs_shp, args.gt_shp, args.deptos_shp, dir_results, args.tile_name)
    run_template(sqlite_out, dbname, quiet=args.quiet)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="Create spatialite script to process tile's shapefiles and return final dataset",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '--sqlite_in',
        help='path of spatialite input template')

    parser.add_argument(
        '--segs_shp',
        help='path of segments shapefile')

    parser.add_argument(
        '--gt_shp',
        help='path of ground truth shapefile')
    
    parser.add_argument(
        '--deptos_shp',
        help='path of deptos shapefile')
    
    parser.add_argument(
        '--tile_name',
        help='Tile name')
    
    parser.add_argument(
        '-q',
        '--quiet',
        default=False,
        action='store_true',
        help="Be quiet")

    args = parser.parse_args()

    main(args)
