import argparse
import pandas as pd
import os
import subprocess
import glob

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--vrt_file', type=str, help="vrt file")
    parser.add_argument('--vec_gt', type=str, help="input ground truth",
                        default="/home/federico/cba1920/pixel_based/buffer_bay_mon_1819.shp")
    args = parser.parse_args()

    tile = args.vrt_file
    print("VRT file {}".format(vrt_file)

    vec_gt = args.vec_gt
    print("input ground truth file {}".format(vec_gt))

    tile_name=os.path.splitext(os.path.basename(tile))[0]
    vec_gt_name, _=os.path.splitext(vec_gt)
    
    poly_stats_name "poly_stats_{tile_name}.xml".format(tile_name=tile_name) 
    cmd_poly_stats = 'otbcli_PolygonClassStatistics -in {tile} -vec {vec_gt} -field crop -out {poly_stats_name}'.format(tile=tile, vec_gt=vec_gt, poly_stats_name=poly_stats_name)
    subprocess.call(cmd_poly_stats, shell=True)
    sample_positions_name = "{tile_name}_sample_positions.sqlite"
    cmd_sample_selection = 'otbcli_SampleSelection -in {tile} -vec {vec_gt} -field crop -strategy -strategy.all -instats {poly_stats_name} -out {sample_positions_name}'.format(tile=tile, vec_gt=vec_gt, poly_stats_name=poly_stats_name, sample_positions_name=sample_positions_name)
    subprocess.call(cmd_sample_selection, shell=True)
    sample_dataset = "{tile_name}_pixel_dataset.sqlite".format(tile_name=tile_name) 
    cmd_dataset = 'otbcli_SampleExtraction -in {tile} -vec {sample_positions_name} -outfield prefix -outfield.prefix.name band_ -field crop -out {sample_dataset}'.format(tile=tile, sample_positions_name=sample_positions_name, sample_dataset=sample_dataset)
    subprocess.call(cmd_dataset, shell=True)
    csv_dataset = "{tile_name}_pixel_dataset.csv".format(tile_name=tile_name) 
    cmd_sqlite_to_csv="ogr2ogr -f CSV {csv} {sqlite}".format(
        csv=csv_dataset, sqlite=sample_dataset)
    subprocess.call(cmd_sqlite_to_csv, shell=True)
    print("Done {vrt}".format(tile))

