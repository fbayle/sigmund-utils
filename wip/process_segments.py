#!/usr/bin/env python3
"""
This script reads a spatialite template, fill them and
export a csv file with segment's spectral attributes
"""

import os
import subprocess
import multiprocessing as mp
from tqdm import tqdm
from glob import glob
import re


def run_processing(tile):
    gt_shp = '/home/federico/cba1920/pixel-based/buffer_bay_mon_1819.shp'
    deptos_shp = '/home/federico/cba1920/deptos_cba.shp'
    tile_path = os.path.splitext(tile)[0]
    tile_name = os.path.basename(tile_path)
    gt_name = os.path.splitext(gt_shp)[0]
    deptos_name = os.path.splitext(deptos_shp)[0]
    if not os.path.exists(tile):
        return
    print('Processing {tile_name}.'.format(tile_name=tile_name))
    cmd = "python3 run_segments_processing.py --quiet --sqlite_in template_segments_join.sqlite --segs_shp {tile_path} --gt_shp {gt_name} --deptos_shp {deptos_name} --tile_name {tile_name}".format(tile_path=tile_path, gt_name=gt_name, deptos_name=deptos_name, tile_name=tile_name)
    subprocess.run(cmd, shell=True)
    
def main(args):
    shps = glob(os.path.join(args.shapefile_dir, '*.shp')

    with mp.Pool() as pool:
        list(tqdm(pool.imap(run_processing, shps), total=len(shps)))

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="Create spatialite script to process tile's shapefiles and return final dataset",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        'shapefile_dir',
        help='Path to shapefile directory')

    args = parser.parse_args()

    main(args)
