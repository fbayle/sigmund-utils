import argparse
import pandas as pd
import os
import subprocess
import glob

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--raster_file', type=str, help="raster file")
    parser.add_argument('--input_fields', type=str, help="input fields level vectorial layer.",
                        default="/home/federico/cba1920/images/2020-02-01_2020-03-01/0000013568-0000027136.shp")
    parser.add_argument('--date', type=str, help="imagery date.",
                        default="/home/federico/cba1920/images/2020-02-01_2020-03-01/0000013568-0000027136.shp")
    args = parser.parse_args()
    date = args.date
    print("Analyzing {}".format(date))
    raster_file = args.raster_file
    print("Raster file {}".format(raster_file))

    input_fields=args.input_fields
    print("input fields file {}".format(input_fields))

    tile=args.raster_file
    print('Tile: {img}'.format(img=tile))
    tile_name=os.path.splitext(os.path.basename(tile))[0]
    path_save = '/home/federico/cba1920/images/{date}'.format(date=date)
    vec_segments=input_fields
    vec_segments_name, _=os.path.splitext(vec_segments)
    vec_zs_name="{vec_segments_name}_{date}.shp".format(
        vec_segments_name=vec_segments_name, date=date)
    otb_zs="otbcli_ZonalStatistics -in {tile_zs_name} -inzone.vector.in {vec_segments} -out.vector.filename {vec_zs_name}".format(
        tile_zs_name=tile, vec_segments=vec_segments, vec_zs_name=vec_zs_name)
    print('Zonal Statistics on {tile_zs_name}, return {vec_zs_name}'.format(
        tile_zs_name=tile, vec_zs_name=vec_zs_name))
    subprocess.call(otb_zs, shell=True)
    csv_zs_name="{vec_segments_name}_{date}.csv".format(
        vec_segments_name=vec_segments_name, date=date)
    cmd_sqlite_to_csv="ogr2ogr -f CSV {csv} {sqlite}".format(
        csv=csv_zs_name, sqlite=vec_zs_name)
    subprocess.call(cmd_sqlite_to_csv, shell=True)
    zs_names.append(csv_zs_name)
    print("Done {vrt}".format(tile))
