import os
import datetime
from dateutil.relativedelta import relativedelta
import subprocess
import shutil

if __name__ == '__main__':


    start = datetime.datetime.strptime("2018-01-01", "%Y-%m-%d")
    end = datetime.datetime.strptime("2019-04-01", "%Y-%m-%d")
    date_generated = [start + relativedelta(months=+x) for x in range(0, 16)]
    dates = [d.strftime("%Y-%m-%d") for d in date_generated]
    dates.append(end.strftime("%Y-%m-%d"))
    dates = [['{d}'.format(d=dates[d]), '{d}'.format(d=dates[d+1])]
             for d in range(0, len(dates)-1)]

    for date in dates:
        cmd_download_images = "gsutil -m cp -r gs://dym-sigmund-temp/cordoba-sigmund-bands-time-series/{date} /home/federico/cba1920/images/".format(
            date=date)
        subprocess.call(cmd_download_images, shell=True)
        vrt_name = '/home/federico/cba1920/images/{date}.vrt'.format(date=date)
        cmd_buildvrt = 'gdalbuildvrt {vrt_name} /home/federico/cba1920/images/*.tif'.format(vrt_name=vrt_name)
        cmd_run_zs = 'python3 extract_features_pixel_based.py --vrt_file {vrt_name}'.format(
            vrt_name=vrt_name)
        subprocess.call(cmd_run_zs, shell=True)
        shutil.rmtree('/home/federico/cba1920/images/{date}'.format(date=date))
        print("{date} done".format(date=date))

