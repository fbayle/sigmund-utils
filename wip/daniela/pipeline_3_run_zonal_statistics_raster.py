import argparse
import pandas as pd
import os
import subprocess
import glob

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--raster_file', type=str, help="raster file")
    parser.add_argument('--input_fields', type=str, help="input fields level vectorial layer.",
                        default="/home/federico/cba_fields/idecor_id.shp")
    parser.add_argument('--date', type=str, help="date")
    parser.add_argument('--output', type=str, help="path output")
    parser.add_argument('--bucket_out', type=str, help="path bucket out")
	
    args = parser.parse_args()

    raster_file = args.raster_file
    print("Raster file {}".format(raster_file))

    input_fields=args.input_fields
    print("input fields file {}".format(input_fields))

    date=args.date
    print("date {}".format(date))
    
    output=args.output
    print("output {}".format(output))

    bucket_out = args.bucket_out
    print("Bucket out {}".format(bucket_out))
	
    tile=args.raster_file
    #print('Tile: {img}'.format(img=tile))
    tile_name=os.path.splitext(os.path.basename(tile))[0]
    tile_names = []
    tile_names.append(tile_name)
    vec_segments=input_fields
    vec_segments_name  = os.path.basename(os.path.splitext(vec_segments)[0])
    vec_zs_name = "{output}{vec_segments_name}_{date}.sqlite".format(
        output=output, vec_segments_name=vec_segments_name, date=date)
    otb_zs="otbcli_ZonalStatistics -in {tile_zs_name} -inzone.vector.in {vec_segments} -out.vector.filename {vec_zs_name}".format(
        tile_zs_name=tile, vec_segments=vec_segments, vec_zs_name=vec_zs_name)
    print('Zonal Statistics on {tile_zs_name}, return {vec_zs_name}'.format(
        tile_zs_name=tile, vec_zs_name=vec_zs_name))
    subprocess.call(otb_zs, shell=True)
    
    csv_zs_name = "{output}{vec_segments_name}_{date}.csv".format(
                    output=output, vec_segments_name=vec_segments_name, date=date)
    cmd_sqlite_to_csv = "ogr2ogr -f CSV {csv} {sqlite}".format(
        csv=csv_zs_name, sqlite=vec_zs_name)
    subprocess.call(cmd_sqlite_to_csv, shell=True)
    
    # ajusta csv agregando campo de referencia
    '''
    nombre_archivo = output+vec_segments_name+'_'+date+'.csv'
    print('Create label_tile '+nombre_archivo)
    datos = pd.read_csv(nombre_archivo)
    datos["label_tile"] = datos["label"].astype(str)+'_'+vec_segments_name
    os.remove(nombre_archivo) 
    datos.to_csv(nombre_archivo, index=False)
    print('Create label_tile done')
    '''
    # ajuste fin

    # copia salidas de local a bucket
    cmd_copy="gsutil cp -r {csv_zs_name} {bucket_out} ".format(
        csv_zs_name=csv_zs_name, bucket_out=bucket_out)
    subprocess.call(cmd_copy, shell=True)		
    cmd_copy="gsutil cp -r {vec_zs_name} {bucket_out} ".format(
        vec_zs_name=vec_zs_name, bucket_out=bucket_out)
    subprocess.call(cmd_copy, shell=True)
		
    print("Done {raster_file}".format(raster_file=raster_file))
