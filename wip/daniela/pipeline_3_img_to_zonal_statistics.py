import os
import datetime
from dateutil.relativedelta import relativedelta
import subprocess
import shutil
import argparse
import glob

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # parser.add_argument('--output_tiles_csv', type=str, help="csv file to save the ouput tiles features",
    #                    default="/home/federico/mosaico_mdv_tiles.csv")
    parser.add_argument('--start', type=str, help="start")
    parser.add_argument('--end', type=str, help="end")
    parser.add_argument('--bucket_images', type=str, help="path bucket")
    parser.add_argument('--output_images', type=str, help="path local images")
    # parser.add_argument('--input_fields', type=str, help="input fields level vectorial layer.",
    #                    default="/home/federico/cba_fields/idecor_id.shp")
    parser.add_argument('--bucket_input_fields', type=str,
                        help="path bucket input fields level vectorial layer")
    parser.add_argument('--input_fields', type=str,
                        help="path local input fields level vectorial layer")
    parser.add_argument('--output', type=str, help="path output")
    parser.add_argument('--bucket_out', type=str, help="path bucket out")

    args = parser.parse_args()

    start = args.start
    print("Start {}".format(start))

    end = args.end
    print("End {}".format(end))

    bucket_images = args.bucket_images
    print("Bucket {}".format(bucket_images))

    output_images = args.output_images
    print("Output images {}".format(output_images))

    bucket_input_fields = args.bucket_input_fields
    print("Bucket Input fields {}".format(bucket_input_fields))

    input_fields = args.input_fields
    print("Input fields {}".format(input_fields))

    output = args.output
    print("Output {}".format(output))

    bucket_out = args.bucket_out
    print("Bucket out {}".format(bucket_out))

    start = datetime.datetime.strptime(start, "%Y-%m-%d")
    end = datetime.datetime.strptime(end, "%Y-%m-%d")

    current = start
    date_generated = []
    while current <= end:
        date_generated.append(current)
        current += relativedelta(months=1)

    dates = [d.strftime("%Y-%m-%d") for d in date_generated]
    dates = [['{d}'.format(d=dates[d]), '{d}'.format(d=dates[d+1])]
           for d in range(0, len(dates)-1)]
    print(dates)

    # copia segmentos de bucket a local
    print("Copy segments")
    cmd_download = "gsutil -m cp -r {bucket_input_fields}* {input_fields}".format(
        bucket_input_fields=bucket_input_fields, input_fields=input_fields)
    #subprocess.call(cmd_download, shell=True)
    tiles = glob.glob('{dir}*.shp'.format(dir=input_fields))
    limit = 50000
    for tile in tiles:
        file_name = os.path.basename(os.path.splitext(tile)[0])
        for date in dates:
            date = '{}_{}'.format(date[0], date[1])
            # copia imagenes de bucket a local
            print("Download images ".format(date=date))
            vec_zs_name = "{output}{vec_segments_name}_{date}.shp".format(
                output=output, vec_segments_name=file_name, date=date)
            cmd_download_images = "gsutil -m cp -r {bucket_images}{date} {output_images}".format(
                bucket_images=bucket_images, date=date, output_images=output_images)
            print(cmd_download_images)
            subprocess.call(cmd_download_images, shell=True)
            raster_name = '{output_images}{d}/{file_name}.tif'.format(
                output_images=output_images, d=date, file_name=file_name)
            cmd_ogrinfo = 'ogrinfo -so -al {tile}'.format(tile=tile) + ' |grep -Eo "Feature Count:.{1,50}" |cut -d : -f 2-'
            n = subprocess.Popen(cmd_ogrinfo, shell=True, stdout=subprocess.PIPE, universal_newlines=True).communicate()[0].strip()
            n = int(n)
            n_last = n % limit
            offsets = [k*limit for k in range(int((n-n_last)/limit)+1)]
            offset_files = []
            tile_sqlite = "'{}'".format(file_name)
            for offset in offsets:
                file_name_chunk = '{output}chunk/{file}_{offset}.shp'.format(
                    output=output, file=file_name, offset=offset)
                tile_sqlite_chunk = "'{file}_{offset}'".format(
                    file=file_name, offset=offset)
                cmd_chunk = 'ogr2ogr -f "ESRI Shapefile" -dialect sqlite -sql "select * from {tile_sqlite} limit {limit} offset {offset}" {file_name_chunk} {tile}'.format(
                    tile_sqlite=tile_sqlite, tile=tile, limit=limit, offset=offset, file_name_chunk=file_name_chunk)
                subprocess.call(cmd_chunk, shell=True)
                chunk_extent = 'ogrinfo -dialect sqlite -sql "select extent(geometry) from {tile_sqlite_chunk}" {file_name} |grep "Extent:"  |cut -d : -f 2- | grep -A 1 -Eo "\(([^)]+)\)"'.format(
                    tile_sqlite_chunk=tile_sqlite_chunk, file_name=file_name_chunk)
                extents = subprocess.Popen(
                    chunk_extent, shell=True, stdout=subprocess.PIPE, universal_newlines=True).communicate()[0].strip()
                mins = extents.split('\n')[0][1:-1].split(',')
                maxs = extents.split('\n')[1][1:-1].split(',')
                xmin, ymin, xmax, ymax = float(mins[0]), float(
                    mins[1]), float(maxs[0]), float(maxs[1])
                clipped_raster_name = '{output}chunk/{file}_{d}_{offset}.tif'.format(
                    output=output, file=file_name, offset=offset, d=date)
                cmd_clip_rastr = 'gdalwarp -te {xmin} {ymin} {xmax} {ymax} -crop_to_cutline -dstalpha {raster_name} {clipped_raster}'.format(
                    xmin=xmin, ymin=ymin, xmax=xmax, ymax=ymax, raster_name=raster_name, clipped_raster=clipped_raster_name)
                subprocess.call(cmd_clip_rastr, shell=True)
                vec_segments_name = os.path.basename(
                    os.path.splitext(file_name_chunk)[0])
                vec_zs_name_chunk = "{output}chunk/{vec_segments_name}_{date}_{offset}.shp".format(
                    output=output, vec_segments_name=file_name, date=date, offset=offset)
                otb_zs = "otbcli_ZonalStatistics -in {raster_name} -inzone.vector.in {vec_segments} -out.vector.filename {vec_zs_name}".format(
                    raster_name=clipped_raster_name, vec_segments=file_name_chunk, vec_zs_name=vec_zs_name_chunk)
                subprocess.call(otb_zs, shell=True)
                offset_files.append(vec_zs_name_chunk)
                offset_files.sort()    
                subprocess.call("ogr2ogr -f 'ESRI Shapefile' {tile_zs} {f}".format(
                    tile_zs=vec_zs_name, f=offset_files[0]), shell=True)
            for shp in offset_files[1:]:
                print("Merge {f}".format(f=shp))
                subprocess.call("ogr2ogr -f 'ESRI Shapefile' -update -append {tile_zs} {f}".format(
                    tile_zs=vec_zs_name, f=shp, tile_name=file_name), shell=True)
            csv_zs_name = '{}.csv'.format(os.path.splitext(vec_zs_name)[0])
            cmd_shp_to_csv = "ogr2ogr -f CSV {csv} {shp}".format(
                csv=csv_zs_name, shp=vec_zs_name)
            subprocess.call(cmd_shp_to_csv, shell=True) 
            # copia salidas de local a bucket
            cmd_copy ="gsutil cp -r {csv_zs_name} {bucket_out} ".format(
                csv_zs_name=csv_zs_name, bucket_out=bucket_out)
            subprocess.call(cmd_copy, shell=True)
            cmd_copy ="gsutil cp -r {vec_zs_name} {bucket_out} ".format(
                vec_zs_name=vec_zs_name, bucket_out=bucket_out)
            subprocess.call(cmd_copy, shell=True)
            # borra recursos intermedios
        shutil.rmtree('{output_images}{date}'.format(output_images=output_images, date=date))
        print("{date} done.".format(date=date))
    print("{tile} done.".format(tile=tile))

