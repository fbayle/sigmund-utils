#!/usr/bin/env python3
"""
Lee archivos sqlite de la ruta sqlite_in
Unifica con join en campo dmx_id
Genera salida csv en out con nombre archivo_out
"""

import sqlite3
import os
import pandas as pd
import csv

def attach_copy(path_sqllite, conn, cursor, tablas):
	print("Agrega bases auxiliares y copia datos")

	archivos = os.listdir(path_sqllite)
	print("Bases "+str(len(archivos)))

	for archivo in archivos:
		tabla = os.path.splitext(archivo)[0]
		print(tabla)
		tablas.append(tabla)
		
		attach = "ATTACH '"+path_sqllite+archivo+"' AS "+tabla
		cursor.execute(attach)
		conn.commit()
		
		# obtiene datos tabla
		comando = "PRAGMA TABLE_INFO("+tabla+")"
		cursor_campos = cursor.execute(comando)

		campos = '';
		comando = "CREATE TABLE bay_"+tabla+"("
		for campo in cursor_campos.fetchall():
			if campo[1] != 'GEOMETRY':
				comando += campo[1]+" "+campo[2]+","
				campos += campo[1]+', ';
			
		# saca ultima coma
		comando = comando[:-1]
		campos = campos[:-2]
		comando += ")"
		
		#print(comando)
		cursor.execute(comando)
		conn.commit()    
		
		indice = "CREATE INDEX idx_"+tabla+" ON bay_"+tabla+" (dmx_id)"
		#print(indice)
		cursor.execute(indice)
		conn.commit()    
		
		comando = "INSERT INTO bay_"+tabla+"("+campos+") \
				   SELECT "+campos+" \
				   FROM "+tabla+"."+tabla
		#print(comando)
		cursor.execute(comando)
		conn.commit()  
		
		attach = "DETACH DATABASE "+tabla
		cursor.execute(attach)
		conn.commit()	

def create_dataset(conn, cursor, tablas):
	print('Genera dataset final')

	comando = "CREATE TABLE bay_dataset as \
			SELECT "
	for tabla in tablas:
		# obtiene datos tabla
		comando_campos = "PRAGMA TABLE_INFO(bay_"+tabla+")"
		cursor_campos = cursor.execute(comando_campos)

		for campo in cursor_campos.fetchall():
			comando += "bay_"+tabla+"."+campo[1]+" as "+tabla+"_"+campo[1]+", "
		
	comando = comando[:-2]    

	comando += " FROM "    
	for tabla in tablas:
		comando += "bay_"+tabla+ ", "     
	comando = comando[:-2]    

	comando += " WHERE "    

	tabla_join = '';
	i = 0
	for tabla in tablas:
		if i == 0:
			tabla_join = "bay_"+tabla
		if i == 1:
			comando += tabla_join+".dmx_id=bay_"+tabla+".dmx_id"
		if i > 1:
			comando += " AND "+tabla_join+".dmx_id=bay_"+tabla+".dmx_id"
		i += 1
		
	#print(comando)
	cursor.execute(comando)
	conn.commit()        

	comando = "SELECT count(*) FROM bay_dataset"
	cursor_total = cursor.execute(comando)
	conn.commit()     
	total = cursor_total.fetchall()

	print("Registros "+str(total[0][0]))

def guarda_dataset(path_salida, archivo_salida, conn, cursor):
	print("Guarda salida")

	query = "SELECT * FROM bay_dataset"
	cursor = cursor.execute(query)

	db_df = pd.read_sql_query(query, conn)
	db_df.to_csv(path_salida+archivo_salida, index=False)
	
def main(args):
	path_sqllite = args.sqlite_in
	path_salida = args.out
	archivo_salida = args.archivo_out
	
	base_principal = 'bayer.db'

	if os.path.exists(path_salida+base_principal):
		os.remove(path_salida+base_principal)
		
	print("Creando base: "+path_salida+base_principal)
	conn = sqlite3.connect(path_salida+base_principal)
	conn.commit()

	tablas = []
	cursor        = conn.cursor()
	
	attach_copy(path_sqllite, conn, cursor, tablas)
	create_dataset(conn, cursor, tablas)
	guarda_dataset(path_salida, archivo_salida, conn, cursor)
	
	conn.close()

	if os.path.exists(path_salida+base_principal):
		os.remove(path_salida+base_principal)
	
if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="Crea dataset unificado a partir de archivos sqlite",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '--sqlite_in',
        help='Ruta completa a los archivos sqlite de entrada')

    parser.add_argument(
        '--out',
        help='Ruta completa donde se genera el dataset')

    parser.add_argument(
        '--archivo_out',
        help="Nombre del dataset final")

    args = parser.parse_args()

    main(args)
