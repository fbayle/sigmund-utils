import os
import datetime
from dateutil.relativedelta import relativedelta
import subprocess
import shutil
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--start', type=str, help="start")
    parser.add_argument('--end', type=str, help="end")
    parser.add_argument('--bucket_images', type=str, help="path bucket in")
    parser.add_argument('--output_images', type=str, help="path local images")
    #parser.add_argument('--input_fields', type=str, help="input fields level vectorial layer.",
    #                    default="/home/federico/cba_fields/idecor_id.shp")
    parser.add_argument('--bucket_input_fields', type=str, help="path bucket input fields level vectorial layer")
    parser.add_argument('--input_fields', type=str, help="path local input fields level vectorial layer")
    parser.add_argument('--output_statistics', type=str, help="path output statistics")
    parser.add_argument('--output_dataset', type=str, help="path output dataset")
    parser.add_argument('--output_name', type=str, help="name dataset")
    parser.add_argument('--bucket_out', type=str, help="path bucket out")
    
    args = parser.parse_args()
    
    start = args.start
    print("Start {}".format(start))

    end = args.end
    print("End {}".format(end))
    
    bucket_images = args.bucket_images
    print("Bucket {}".format(bucket_images))
    
    output_images = args.output_images
    print("Output images {}".format(output_images))
    
    bucket_input_fields = args.bucket_input_fields
    print("Bucket Input fields {}".format(bucket_input_fields))

    input_fields = args.input_fields
    print("Input fields {}".format(input_fields))
    
    output_statistics = args.output_statistics
    print("output statistics {}".format(output_statistics))
    
    output_dataset = args.output_dataset
    print("output dataset {}".format(output_dataset))
    
    output_name = args.output_name
    print("Name dataset {}".format(output_name))

    bucket_out = args.bucket_out
    print("Bucket out {}".format(bucket_out))
    
    print("pipeline_3_img_to_zonal_statistics")
    cmd_run='python3 pipeline_3_img_to_zonal_statistics.py --start {start} --end {end} --bucket_images {bucket_images} --output_images {output_images} --bucket_input_fields {bucket_input_fields} --input_fields {input_fields} --output {output_statistics} --bucket_out {bucket_out}'.format(
            start=start, end=end, bucket_images=bucket_images, output_images=output_images, bucket_input_fields=bucket_input_fields, input_fields=input_fields, output_statistics=output_statistics, bucket_out=bucket_out)
    subprocess.call(cmd_run, shell=True)
    print("done pipeline_3_img_to_zonal_statistics")
    
    #print("pipeline_3_create_dataset_from_csv")
    #cmd_run='python3 pipeline_3_create_dataset_from_csv.py --path_in {output_statistics} --path_out {output_dataset} --archivo_out {output_name}'.format(
    #        output_statistics=output_statistics, output_dataset=output_dataset, output_name=output_name)
    #subprocess.call(cmd_run, shell=True)
    #print("done pipeline_3_create_dataset_from_csv")
    