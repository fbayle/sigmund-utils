#!/usr/bin/env python3
"""
Lee archivos csv de la ruta csv_in, con nombre idecor_id_2017-01-01_2017-02-01.csv
Unifica con join en campo dmx_id
Genera salida csv en out con nombre archivo_out

Genera periodos.csv con el mapeo de los
periodos procesados y sus fechas
"""

import os
import pandas as pd

def adapta_nombres_campos(campos_originales, p):
    # p[i]_[campo]_[banda]    
    datos_columnas = []
    
    for campo in campos_originales:
        #print(campo)
        sufijo = "p"+str(p)+"_"
        if (campo == 'dmx_id' or campo == 'crop'):
            # queda igual
            datos_columnas.append(campo)
        else:
            if (campo == 'count'):
                campo = sufijo+campo
            else:
                datos_campo = campo.split('_')
                banda_id = int(datos_campo[1])

                banda = banda_id_to_str(banda_id)
                campo = sufijo+datos_campo[0]+"_"+banda
                
            datos_columnas.append(campo)
            
    return datos_columnas

def banda_id_to_str(banda_id):
    # bandas
    # 0 b2
    # 1 b3
    # 2 b4
    # 3 b8
    # 4 b11
    # 5 b12
    banda = ''
    
    if (banda_id == 0):
        banda = 'b2'
    if (banda_id == 1):
        banda = 'b3'
    if (banda_id == 2):
        banda = 'b4'
    if (banda_id == 3):
        banda = 'b8'
    if (banda_id == 4):
        banda = 'b11'
    if (banda_id == 5):
        banda = 'b12'
        
    return banda
    
def main(args):
    path_in = args.path_in
    path_salida = args.path_out
    archivo_salida = args.archivo_out
        
    archivos = sorted(os.listdir(path_in))
    print("Archivos "+str(len(archivos)))

    dataset = pd.DataFrame()

    periodos_columnas = ['periodo', 'fecha_inicio', 'fecha_fin']
    periodos = pd.DataFrame(columns = periodos_columnas)

    p = 1
    for archivo in archivos:
        if archivo.endswith(".csv"):
            print(archivo)
            
            # datos periodo
		    # [pueden_ser_varios]_2017-01-01_2017-02-01.csv
            datos_periodo = archivo.split('_')
            fecha_inicio = datos_periodo[-2]
            fecha_fin = datos_periodo[-1][:-4]
            
            periodo = pd.DataFrame([[p, fecha_inicio, fecha_fin]], columns = periodos_columnas)
            periodos = periodos.append(periodo, ignore_index=True) 
    
            datos = pd.read_csv(path_in+archivo)
            #print(datos.shape)
        
            # adapta nombres
            # p[i]_[campo]_[banda]
            campos_originales = datos.columns.tolist()
            datos.columns = adapta_nombres_campos(campos_originales, p)
            
            # join por dmx_id: id del lote
            if (p==1):
                dataset = datos
            else:
                dataset = pd.merge(dataset, datos, on='dmx_id') 
                #pd.concat([dataset, datos], ignore_index=True, sort=False)
            
            p += 1

    #campos = dataset.columns.tolist()
    #print(campos)

    #print(dataset.shape)  
    
    # salidas
    print('Genera csv periodos')
    periodos.to_csv(path_salida+'periodos.csv', index=False)
    print('Genera dataset')
    dataset.to_csv(path_salida+archivo_salida, index=False)
    
if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="Crea dataset unificado a partir de archivos csv",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '--path_in',
        help='Ruta completa a los archivos csv de entrada')

    parser.add_argument(
        '--path_out',
        help='Ruta completa donde se genera el dataset')

    parser.add_argument(
        '--archivo_out',
        help="Nombre del dataset final")

    args = parser.parse_args()

    main(args)
