import argparse
import pandas as pd
import os
import subprocess
import glob

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--vrt_file', type=str, help="vrt file")
    parser.add_argument('--input_fields', type=str, help="input fields level vectorial layer.",
                        default="/home/federico/cba_fields/idecor_id.shp")
    parser.add_argument('--output', type=str, help="path output")
    args = parser.parse_args()

    vrt_file = args.vrt_file
    print("VRT file {}".format(vrt_file))

    input_fields=args.input_fields
    print("input fields file {}".format(input_fields))

    output=args.output
    print("output {}".format(output))

    tile=args.vrt_file
    #print('Tile: {img}'.format(img=tile))
    tile_name=os.path.splitext(os.path.basename(tile))[0]
    tile_names = []
    tile_names.append(tile_name)
    vec_segments=input_fields
    vec_segments_name  = os.path.basename(os.path.splitext(vec_segments)[0])
    vec_zs_name = "{output}{vec_segments_name}_{tile_name}.sqlite".format(
        output=output, vec_segments_name=vec_segments_name, tile_name=tile_name)
    otb_zs="otbcli_ZonalStatistics -in {tile_zs_name} -inzone.vector.in {vec_segments} -out.vector.filename {vec_zs_name}".format(
        tile_zs_name=tile, vec_segments=vec_segments, vec_zs_name=vec_zs_name)
    print('Zonal Statistics on {tile_zs_name}, return {vec_zs_name}'.format(
        tile_zs_name=tile, vec_zs_name=vec_zs_name))
    subprocess.call(otb_zs, shell=True)
    csv_zs_name = "{output}{vec_segments_name}_{tile_name}.csv".format(
                    output=output, vec_segments_name=vec_segments_name, tile_name=tile_name)
    cmd_sqlite_to_csv = "ogr2ogr -f CSV {csv} {sqlite}".format(
        csv=csv_zs_name, sqlite=vec_zs_name)
    subprocess.call(cmd_sqlite_to_csv, shell=True)
    print("Done {vrt_file}".format(vrt_file=vrt_file))
