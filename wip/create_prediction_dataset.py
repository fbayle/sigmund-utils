import os
import datetime
from dateutil.relativedelta import relativedelta
import subprocess
import shutil
import multiprocessing as mp
from tqdm import tqdm
from itertools import repeat
import pandas as pd
import glob

if __name__ == '__main__':

    start = datetime.datetime.strptime("2019-01-01", "%Y-%m-%d")
    end = datetime.datetime.strptime("2020-03-01", "%Y-%m-%d")
    date_generated = [start + relativedelta(months=+x) for x in range(0, 14)]
    dates = [d.strftime("%Y-%m-%d") for d in date_generated]
    dates.append(end.strftime("%Y-%m-%d"))
    dates = [['{d}'.format(d=dates[d]), '{d}'.format(d=dates[d+1])]
             for d in range(0, len(dates)-1)]
    bands = [1, 2, 3, 4, 5, 6]

    def worker(band, date):
        #date = '{d1}_{d2}'.format(d1=date[0], d2=date[1])
        path_in = '/home/federico/cba1920/images/{date}'.format(date=date)
        path_out_nan = '/home/federico/cba1920/images/test-pixel/tmp'
        path_out = '/home/federico/cba1920/images/test-pixel/final'
        os.makedirs(path_out, exist_ok=True)
        filename_nan = '{path}/0000013568-0000027136_{band}_{date}.csv'.format(
            path=path_out_nan, band=band, date=date)
        print(os.path.isfile(filename_nan))
        if not os.path.isfile(filename_nan):
            cmd = 'gdal_translate -b {band} -of xyz -co ADD_HEADER_LINE=YES -co COLUMN_SEPARATOR="," {path}/0000013568-0000027136.tif {filename_nan}'.format(
                band=band, filename_nan=filename_nan, path=path_in)
            subprocess.call(cmd, shell=True)
        filename = '{path}/0000013568-0000027136_{band}_{date}.csv'.format(
        path=path_out, band=band-1, date=date)
        #chunksize = 10 ** 3
        #df_l = []
        #for chunk in pd.read_csv(filename_nan, chunksize=chunksize):
        #    chunks_exp = chunk.dropna()
        #    df_l.append(chunks_exp)
        #print(len(df_l))
        #df = pd.concat(df_l, sort=False)
        #df.columns = ['X', 'Y', 'band_{b}'.format(b=band-1)]
        #df.to_csv(filename, index=False)
        #filename_zip = '{path}/0000013568-0000027136_{band}_{date}.zip'.format(
        #    path=path_out, band=band-1, date=date)
        #cmd_zip = 'zip {filename_zip} {filename}'.format(filename_zip=filename_zip, filename=filename_nan)
        #subprocess.call(cmd_zip, shell=True)
        #cmd_gsutil_up = 'gsutil cp {filename_zip} gs://dym-sigmund-temp/cordoba-sigmund-bands-time-series-pixel/{date}/{filename_zip}'.format(filename_zip=filename_zip, date=date)
        #subprocess.call(cmd_gsutil_up, shell=True)
        cmd_grep = 'rg -F -v nan {filename_nan} > {filename}'.format(filename_nan=filename_nan, filename=filename)
        subprocess.call(cmd_grep, shell=True)
        #os.remove(filename_nan)
        if band == 1:
            print('Processing band {}'.format(band))
            subprocess.call("sed -i -r '1s/Z/band_{0}/g' {1} ".format(band-1, filename), shell=True)
        else:
            filename_band = '{path}/0000013568-0000027136_{band}_{date}_band.csv'.format(
                                                    path=path_out, band=band-1, date=date)
            print('Processing band {}'.format(band))
            subprocess.call('cut -d, -f1,2 --complement {} > {}'.format(filename, filename_band), shell=True)
            subprocess.call("sed -i -r '1s/Z/band_{}/g' {} ".format(band-1,filename_band), shell=True)
    #l = []
    #for date in dates:
        #date = '{d1}_{d2}'.format(d1=date[0], d2=date[1])
        #os.makedirs('/home/federico/cba1920/images/{date}'.format(date=date), exist_ok=True)
        #cmd_gsutil = 'gsutil -m cp gs://dym-sigmund-temp/cordoba-sigmund-bands-time-series/{date}/0000013568-0000027136.tif /home/federico/cba1920/images/{date}/'.format(date=date)
        #subprocess.call(cmd_gsutil, shell=True)
        #for band in bands:
        #    l.append((band, date))
    l = ['{}_{}'.format(d[0], d[1]) for d in dates]   
    for date in l[1:]:
        with mp.Pool(6) as pool:
            res = list(tqdm(pool.starmap(worker, zip(bands, repeat(date)))))
        path_out_monthly = '/home/federico/cba1920/images/test-pixel/monthly'
        path_out_nan = '/home/federico/cba1920/images/test-pixel/tmp'
        path_out = '/home/federico/cba1920/images/test-pixel/final'
        print('Saving montly file {}'.format(date))
        filename_date = '{path}/0000013568-0000027136_{date}.csv'.format(path=path_out_monthly, date=date)        
        fnames_bands = ['{path}/0000013568-0000027136_0_{date}.csv'.format(
                                                                            path=path_out, date=date)] + glob.glob('{}/*_band.csv'.format(path_out))
        fnames_bands.sort()
        subprocess.call('paste -d, {} {} {} {} {} {} > {}'.format(fnames_bands[0],fnames_bands[1],fnames_bands[2],fnames_bands[3],fnames_bands[4],fnames_bands[5],filename_date), shell=True)
        print('Cleaning dirs...')
        tmps = glob.glob('{path}/*.csv'.format(path=path_out_nan))
        for f in tmps:
            os.remove(f)
        finals = glob.glob('{path}/*.csv'.format(path=path_out))
        for f in finals:
            os.remove(f)
        print('Done {}.'.format(date))

       
