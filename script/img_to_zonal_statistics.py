import os
import datetime
from dateutil.relativedelta import relativedelta
import subprocess
import shutil
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--output_tiles_csv', type=str, help="csv file to save the ouput tiles features",
                        default="/home/federico/mosaico_mdv_tiles.csv")

    start=datetime.datetime.strptime("2017-01-01", "%Y-%m-%d")
    end=datetime.datetime.strptime("2018-01-01", "%Y-%m-%d")
    date_generated=[start + relativedelta(months=+x) for x in range(0, 13)]
    dates=[d.strftime("%Y-%m-%d") for d in date_generated]
    dates.append(end.strftime("%Y-%m-%d"))
    dates=[['{d}'.format(d=dates[d]), '{d}'.format(d=dates[d+1])]
            for d in range(0, len(dates)-1)]
    print(dates)
    for date in dates:
        date = '{}_{}'.format(date[0], date[1])
        cmd_download_images="gsutil -m cp -r gs://dym-sigmund-temp/cordoba-sigmund-bands-time-series/{date} /home/federico/cba1920/images/".format(
            date=date)
        subprocess.call(cmd_download_images, shell=True)
        vrt_name='/home/federico/cba1920/images/{date}.vrt'.format(date=date)
        cmd_buildvrt= 'gdalbuildvrt  {vrt_name} /home/federico/cba1920/images/{d}/*.tif'.format(vrt_name=vrt_name, d=date)
        print(cmd_buildvrt)
        subprocess.call(cmd_buildvrt, shell=True)
        cmd_run_zs='python3 run_zonal_statistics.py --vrt_file {vrt_name}'.format(
            vrt_name=vrt_name)
        subprocess.call(cmd_run_zs, shell=True)
        #shutil.rmtree('/home/federico/cba1920/images/{date}'.format(date=date))
        print("{date} done".format(date=date))
